const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/', (req, res) => {
  const path = "./messages";
  let messageArray = fs.readdirSync(path);
  console.log('initial' ,messageArray);
  let fiveMessages;
  if(messageArray.length>5){
  fiveMessages = messageArray.slice(-5);
  } else{
    fiveMessages = messageArray;
  }

 const fiveResponse=[];
 fiveMessages.forEach(file=>{
    const data = fs.readFileSync('./messages/'+file);
    console.log(data)
    const message = JSON.parse(data);
    fiveResponse.push(message);
  })
  res.send(fiveResponse);
});

router.post('/', (req, res) => {
  const date = new Date();
  const currentDate = date.toISOString();
  const fileName = './messages/'+currentDate+'.json';
  const response = {message: req.body.message, datetime: currentDate};
  const stringified = JSON.stringify(response);

  fs.writeFile(fileName, stringified, (err) => {
    if (err) {
      console.error(err);
    } else{
      console.log('File was saved!');
    }

  });

  res.send(stringified);
});


module.exports = router;